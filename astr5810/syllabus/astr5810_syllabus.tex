%%% Title of Document 
\documentclass{tufte-handout}

% \geometry{showframe} % (for debugging, shows lines of frames)


\hypersetup{colorlinks=true}

% preamble
\definecolor{orange}{rgb}{.8,0.4,0}
\definecolor{gray}{rgb}{.5,0.5,0.5}
\definecolor{editcolor}{rgb}{0.6,0.0,0}
\newcommand{\question}[1]{\sffamily {\em {\color{orange} [#1]}}\normalfont}
\newcommand{\todo}[1]{\sffamily {\em {\color{orange} #1}}\normalfont}
\newcommand{\link}[2]{\href{#1}{#2}}



%%% set the title
\title{ASTR/GEOL/ATOC 5810: Planetary Atmospheres}
\author{\LARGE Syllabus for Fall 2017}
\date{}


\begin{document}
\maketitle

~

Prof. Zach Berta-Thompson \\
\url{zach.bertathompson@colorado.edu} \\
Office: Duane D213, 303-735-6821 \\


\subsection{Why would you take this course?}
This is primarily a class about how planetary atmospheres work. It provides a basic overview of the landscape of our current understanding of the atmospheres of Earth, of other bodies in the Solar System, and of known or hypothetical exoplanets. We will apply techniques of astronomy, physics, chemistry, geosciences, fluid dynamics, and data visualization to understand the general processes that sculpt planetary atmospheres. Whether you are exploring a new topic that piqued your interest or you are already passionate about a research career in the field of planetary atmospheres, all I ask is that you approach this class with a sense of curiosity.

\subsection{Course Content}
The scientific concepts covered in this class include the structure, composition, dynamics, and evolution of planetary atmospheres. We will cover the origin of planetary atmospheres, basic thermodynamics, radiative transfer, greenhouse effects, chemistry and cloud physics, climate, and atmospheric escape. 

\subsection{Learning Goals}
\noindent
This class is targeted at early-career graduate students with an interest in planetary atmospheres. Our goals for the class include practicing useful grad school skills, in addition to scientific topics.

\begin{itemize}
\setlength\itemsep{0mm}
\item Develop familiarity with basic principles governing atmospheric structure, climate, dynamics, evolution.
\item Practice combining analytic problem-solving with numerical simulation and data visualization.
\item Read classic and current scientific literature, as a means to explore topics of planetary atmospheres.
\item Exercise scientific communication skills, through writing and oral presentations.
\item Gain expertise working collaboratively in teams. 
\end{itemize}

\subsection{Course Preparation}
This course has no formal pre-requisites. However, a general background in undergraduate-level astronomy, physics, chemistry, and/or geosciences will be a substantial help. As most class projects will rely on coding, experience with scientific coding will be extremely helpful. If you have concerns about your background with specific topics, please come talk to me.


\subsection{Class Time and Location}
We meet 1:00-1:50 PM MWF in Duane E126.

\subsection{Office hours}
I am usually in my office with the door open. Feel free to drop by anytime, email me to set up a specific time to meet, or ask questions on {\tt slack}.

\subsection{Course Logistics}
The class website is \url{zkbt.bitbucket.io/astr5810}. Assignments, datasets, and other materials will be posted here. I have also set up a course {\tt slack} team, where we can discuss and share outside of the classroom. You'll receive an invitation if you are registered for the class; if you're not registered but would still like to participate on {\tt slack}, please let me know.




\subsection{General Course Format}
\begin{tabular}{rl}
{\bf Mon.} 		& Conceptual basics through traditional lecture format.\\
{\bf Wed.} 		& Hands-on application of concepts (bring your laptop!) \\
{\bf Fri.} 		& Critical reading of literature papers. \\
\end{tabular}

\subsection{Grades }

\begin{description}
\item{15\%} Class participation (contributions to in-class and online discussions, and the promotion of an inclusive learning environment).
\item{50\%} Weekly projects (including written responses, analytic solutions, plots, and code).
\item{10\%} Oral paper presentation (leading a class discussion of a journal article from the planetary atmospheres literature). 
\item{25\%} Semester-long project (a written and oral presentation of an inquiry-driven research project).
\end{description}



\subsection{Textbooks}

\begin{itemize}
\item {\bf \underline{Planetary Climates}, Andrew P. Ingersoll, 2013, (Princeton University Press) --} This relatively inexpensive text is a concise and readable survey of Solar System planetary atmospheres and governing processes. If you read this book closely and dig deeply into the concepts it introduces, you will have learned a lot. If you are considering a more in-depth research career in planetary atmospheres, it may be worth it for you to invest in one of the following books to serve as a more detailed reference on equations, derivations, and techniques. This is the only ``required'' book.

\item {\bf \underline{Principles of Planetary Climate}, Raymond T. Pierrehumbert, 2010, (Cambridge University Press).} -- This book pays exceptional attention to its pedagogical approach, with a nice balance of explanations and equations. I will be drawing from this book for class exercises. 

\item {\bf \underline{An Introduction to Planetary Atmospheres}, Agust\'{\i}n S\'anchez-Lavega, 2010, (CRC Press/Taylor \& Francis)} -- This is a good general reference for planetary atmospheres, focused on Solar System planets but with fairly broad coverage across sub-topics.

\item {\bf \underline{Exoplanet Atmospheres: Physical Processes}, Sara Seager, 2010, (Princeton University Press).} -- This is the "classic" text in exoplanet atmospheres, and a solid general reference, particularly for topics related to radiation and spectroscopy.

\item {\bf \underline{Exoplanet Atmospheres: Theoretical Concepts and Foundations}, Kevin Heng, 2017, (Princeton University Press).} -- This newer text is a great complement to Seager's, particularly filling in on general principles of atmospheric fluid dynamics (in an exoplanet context). 

\item {\bf \underline{Atmospheric Evolution on Inhabited and Lifeless Worlds}, David C. Catling and James F. Kasting, 2017, (Cambridge University Press)} -- This comprehensive book is a good source for numerous topics in planetary atmospheres, with (as the title suggestions) a particular focus on long-term atmospheric evolution.

\end{itemize}

\subsection{Attendance and Deadlines}
I expect you to attend and participate constructively in class. If you miss class, you will be responsible for completing work that you missed and gathering notes to catch up. I will accommodate reasonable requests for extensions on deadlines, particularly if you make arrangements with me before the deadline passes. 

\subsection{Students with Disabilities}
I do not want any disabilities to stand in the way of your learning experience in this class, and I am happy to accommodate your needs. Whether you have a letter from Disability Services documenting a disability need or not, please set up a time to talk to me about how I can help.

\subsection{Observance of Religious Holidays}
Campus policy regarding religious observances requires that faculty make every effort to deal reasonably and fairly with all students who, because of religious obligations, have conflicts with scheduled exams, assignments or required attendance. In this class, if you must miss an exam, assignment, or tutorial because of observance of a religious holiday, please notify me in writing at least one week prior.  See the \link{http://www.colorado.edu/policies/observance-religious-holidays-and-absences-classes-andor-exams}{campus policy regarding religious observances} for full details.

\subsection{Respect in the Classroom}
You and I each have responsibility for maintaining an appropriate learning environment. Those who fail to adhere to such behavioral standards may be subject to discipline. Professional courtesy and sensitivity are especially important with respect to individuals and topics dealing with differences of race, color, culture, religion, creed, politics, veteran's status, sexual orientation, gender, gender identity and gender expression, age, disability, and nationalities. Class rosters are provided to me with your legal name. I will gladly honor your request to address you by an alternate name or gender pronoun. Please advise me of this preference early in the semester so that I may make appropriate changes to my records. For more information, see the policies on \link{http://www.colorado.edu/policies/student-classroom-and-course-related-behavior}{classroom behavior} and the \link{http://www.colorado.edu/osc/sites/default/files/attached-files/studentconductcode_16-17-a.pdf}{student code}.

\subsection{Honor Code}
All students enrolled in a University of Colorado Boulder course are responsible for knowing and adhering to the \link{http://www.colorado.edu/policies/academic-integrity-policy}{academic integrity policy} of the institution. Violations of the policy may include: plagiarism, cheating, fabrication, lying, bribery, threat, unauthorized access, clicker fraud, resubmission, and aiding academic dishonesty. All incidents of academic misconduct will be reported to the Honor Code Council (\url{honor@colorado.edu}; 303-735-2273). Students who are found responsible for violating the academic integrity policy will be subject to nonacademic sanctions from the Honor Code Council as well as academic sanctions from the faculty member. Additional information regarding the academic integrity policy can be found at \url{honorcode.colorado.edu}.

\subsection{Discrimination and Sexual Harassment}
CU Boulder is committed to maintaining a positive learning, working, and living environment. CU Boulder will not tolerate acts of sexual misconduct, discrimination, harassment or related retaliation against or by any employee or student. CU's Sexual Misconduct Policy prohibits sexual assault, sexual exploitation, sexual harassment, intimate partner abuse (dating or domestic violence), stalking or related retaliation. CU Boulder's Discrimination and Harassment Policy prohibits discrimination, harassment or related retaliation based on race, color, national origin, sex, pregnancy, age, disability, creed, religion, sexual orientation, gender identity, gender expression, veteran status, political affiliation or political philosophy. Individuals who believe they have been subject to misconduct under either policy should contact the Office of Institutional Equity and Compliance (OIEC) at 303-492-2127. Information about the OIEC, the above referenced policies, and the campus resources available to assist individuals regarding sexual misconduct, discrimination, harassment or related retaliation can be found at the \link{http://www.colorado.edu/institutionalequity/}{OIEC website}.

%\subsection{Approximate Schedule}
%\noindent
%It's entirely possible we'll deviate quite a bit from this!


\clearpage
%% 
% LECTURES
%
%

%
%\begin{table}[ht]
%\centering
%\noindent {\it Very Approximate} Lecture and Reading Schedule
%\begin{tabular}{cllc}
%\hline\hline
%Lecture & Date & Topic & Textbook Reading \\
%\hline
%0 & Aug 23 & Introduction & ... \\
%1 & Aug 25 & Linux & ... \\
%2 & Aug 20 & iPython, Data Types, Print & 1.1--1.2 \\
%3 & Sep 1 & Formulas \& Terminology, Modules & 1.3--1.5 \\
%4 & Sep 6 & While Loops, Boolean Expressions & 2.1 \\
%5 & Sep 8 &  For Loops, Lists & 2.2--2.3 \\
%6 & Sep 13 & Nested Lists \& Sublists & 2.4 \\
%7 & Sep 15 & Tuples \& Functions & 2.5, 3.1 \\
%8 & Sep 20 &  Functions \& Branching & 3.2 \\
%9 & Sep 22 & More on Functions & ... \\
%10 & Sep 27 & Vectors, numpy & 5.1 \\ 
%11 & Sep 29 & Arrays \& Plotting, Boolean Indexing & 5.2--5.3 \\
%12 & Oct 4 & Arrays \& Plotting (118 years of Boulder weather) & 5.2--5.3\\
%13 & Oct 6 & Arrays \& Plotting (2013 Bolder Boulder results) & 5.2--5.3\\
%14 & Oct 11 & Numerical Differentiation & Appendix B.2 \\
%15 & Oct 13 & Numerical Integration & Appendix B.3 \\
%16 & Oct 18 & Modules \& Packages & 4.9\\ 
%17 & Oct 20 & Error Handling & 4.7 \\ 
%18 & Oct 25 &  Reading Data from \& Writing Data to a File & 4.5--4.6 \\ 
%19 & Oct 27 & Multidimensional Arrays & 5.7 \\  
%20 & Nov 1 & Multidimensional Arrays (Spiral galaxy analysis) & ... \\
%21 & Nov 3 & Random Numbers & 8.1--8.2 \\
%22 & Nov 8 & Monte Carlo Simulations (Random walks) & 8.3 \\ 
%23 & Nov 10 & Dictionaries \& Strings & 6.1 \& 6.2 \\ 
%24 & Nov 15 & Classes & 7.1 \\
%25 & Nov 17 & Object-Oriented Programming & 9.1 \\
%... & Nov 22 & {\it Fall Break} & ... \\
%... & Nov 24 & {\it Fall Break} & ... \\
%26  & Nov 29 & Object-Oriented Programming & 9.1 \\ 
%27 & Dec 1 & Source Control (git/svn) & ... \\
%28 & Dec 6 & astropy & ...\\
%29 & Dec 8 & Special Topic & ... \\
%\hline
%\end{tabular}
%\end{table}
%
%%
% END LECTURES
%

\end{document}



